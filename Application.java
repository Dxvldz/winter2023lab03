public class Application {
    public static void main(String[]args){
        Student kevin = new Student();
        Student jaypee = new Student();

        // In our database, we only have kevin,jaypee and Lev as student.
        kevin.name = "Kevin";
        jaypee.name = "Jaypee";

        kevin.averageGrade = 92;
        jaypee.averageGrade = 54;

        kevin.worstGrade = 89;
        jaypee.worstGrade = 12;

        kevin.classCrush = "Tony";
        jaypee.classCrush = "Yejun";

        kevin.favoriteHobby = "fencing";
        jaypee.favoriteHobby = "basketball";

        kevin.favoriteSubject = "Math";
        jaypee.favoriteSubject = "Diner";

        Student[] section3 = new Student[3];
        section3[0] = kevin;
        section3[1] = jaypee;
        section3[2] =  new Student();

        section3[2].classCrush = "Hasani";
        section3[2].name = "Lev";
        section3[2].averageGrade = 74;
        section3[2].worstGrade = 23;
        section3[2].favoriteHobby = "Sleeping";
        section3[2].favoriteSubject ="Home";


     System.out.println(section3[0].name);
     System.out.println(section3[2].name);
     System.out.println(kevin.averageGrade);
     System.out.println(kevin.bestAverage(kevin.averageGrade,jaypee.averageGrade,kevin.name,jaypee.name));
    }
}